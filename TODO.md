# Right now

[ ] site logic
[ ] database with users
[ ] google maps integration
[ ] user authentication

# Later

[ ] messaging capability
[ ] set up FAQ
[ ] privacy and terms
[ ] disclaimer

# Some Day

[ ] UserVoice support
[ ] user groups
[ ] popular runs
[ ] paid subscriber model (Ƀ/$/y)