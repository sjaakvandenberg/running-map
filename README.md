# Readme
----

This app uses the Sass variant of the [Semantic UI](http://semantic-ui.com/) framework (gem `semantic-ui-sass`), together with [Font Awesome](http://fontawesome.io/) icons.

# Run locally
----

    $ git clone git@bitbucket.org:sjaakvandenberg/running-map.git
    $ bundle install
    $ rake server

Tested on Ruby 2.2.2p95 with Rails 4.2.1. Uses a SQLite3 database.

# Running Map
----

![screenshot](screenshot.png)
_Conceptual screenshot_

The project's name shall be 'Running Map', until we can come up with a better one. The goal of the app is to provide people with an easy way to find others in their area to run with.

This app is developed by [@janetalkstech](https://twitter.com/janetalkstech) and [@svdb](https://twitter.com/svdb).

# The Map
----

## Google Maps

Option 1: Use the `gmaps4rails` gem to interact with the Google Maps API. Here's a [tutorial](http://www.sitepoint.com/use-google-maps-rails/). This requires the `underscore` library.

## Mapbox

Option 2 is [Mapbox](https://www.mapbox.com/), which is free for 50k views/month. Can be [easily integrated](http://vladigleba.com/blog/2013/11/14/using-mapbox-with-ruby-on-rails/) in Rails.

# Database Structure
----

User:

    * ID (int)
    * first_name (string)
    * last_name (string)
    * username (string)
    * password
    * lat (float)
    * lng (float)
    * skill_level (int)
    * run_length (int)
    * member_of (array)
    * picture (bin)
    * bio (string)
    * availability (nested array)

# Pages
----

## Home

Functionality and user experience are key. Instead of introductory pages, the functionality is apparent right when the user opens the page. It's a Google Maps interface, which shows a map based on the user's location (using the user's IP). The map should already show nearby runners.

Navigating the map can be done by using the search function, which will take the same input as the Google Maps API does (ZIP codes, City or street names, etc).

To encourage participation, a clear call to action to become a member should be displayed on this page.

The markers on the map should display the runner's profile, when clicked.

## Your Profile

Runners can edit their availability, skill level, upload a picture, delete their profile, add information, etc.

## Messages

Simple messaging interface. Users can block others and delete, view and reply to previous messages.

## FAQ

Frequently asked questions

## UserVoice support page

Some day.

## Privacy and terms

We'll probably leave your data alone.

## Disclaimer

We're not responsible.